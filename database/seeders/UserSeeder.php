<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'name' => 'Doni',
            'email' => 'doni@gmail.com',
            'password' => Hash::make('123456'),
            'nip' => '1234',
            'role_id' => 1,
            'is_active' => 1
        ]);
        User::create([
            'name' => 'Dono',
            'email' => 'dono@gmail.com',
            'password' => Hash::make('123456'),
            'nip' => '1235',
            'role_id' => 2,
            'is_active' => 1
        ]);
        User::create([
            'name' => 'Dona',
            'email' => 'dona@gmail.com',
            'password' => Hash::make('123456'),
            'nip' => '1236',
            'role_id' => 3,
            'is_active' => 1
        ]);
    }
}