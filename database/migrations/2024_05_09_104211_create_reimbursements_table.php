<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('reimbursements', function (Blueprint $table) {
            $table->id();
            $table->dateTime('date');
            $table->string('name');
            $table->text('desc')->nullable();
            $table->string('file')->nullable();
            $table->unsignedBigInteger('created_by_staff')->nullable();
            $table->boolean('is_approved_by_director')->nullable();
            $table->unsignedBigInteger('approved_by_director')->nullable();
            $table->text('note_director')->nullable();
            $table->boolean('is_approved_by_finance')->nullable();
            $table->unsignedBigInteger('approved_by_finance')->nullable();
            $table->text('note_finance')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('reimbursements');
    }
};
