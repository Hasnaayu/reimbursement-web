@extends('layouts.master')
@section('contents')
    <form class="form content-padding" method="POST" id="update_reimbursement"
        action="{{ url('/dashboard/reimbursement/update/' . $reimbursement->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        @include('layouts.alert')

        @if (auth()->user()->role_id == 3)
            <div class="card card-custom pb-5 mt-10">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h2 class="card-label font-weight-bold">Data Reimbursement
                            <span class="d-block text-muted pt-2 font-size-sm">Masukkan data reimbursement baru</span>
                        </h2>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group row align-items-center mt-2">
                        <div class="col-lg-3">
                            <h6 class="font-weight-bold">Tanggal</h6>
                        </div>
                        <div class="col-lg-9">
                            <div class="input-group date" id="kt_datetimepicker_2" data-target-input="nearest">
                                <div class="input-group-prepend" data-target="#kt_datetimepicker_2"
                                    data-toggle="datetimepicker">
                                    <span class="input-group-text" style="background-color: transparent">
                                        <i class="ki ki-calendar base_color"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control-datetime datetimepicker-input"
                                    placeholder="Select date & time" data-target="#kt_datetimepicker_2" name="date"
                                    required value="{{ $reimbursement->date }}" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group row align-items-center mt-2">
                        <div class="col-lg-3">
                            <h6 class="font-weight-bold">Nama</h6>
                        </div>
                        <div class="col-lg-9">
                            <input type="text" name="name" class="form-control" value="{{ $reimbursement->name }}"
                                placeholder="Masukkan nama dengan benar" required />
                        </div>
                    </div>
                    <div class="form-group row align-items-center mt-2">
                        <div class="col-lg-3">
                            <h6 class="font-weight-bold">Deskripsi</h6>
                        </div>
                        <div class="col-lg-9">
                            <textarea class="form-control" name="desc" rows="6" cols="30" style="text-align: justify">{{ $reimbursement->desc }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row align-items-center mt-2">
                        <div class="col-lg-3">
                            <h6 class="font-weight-bold">File Pendukung</h6>
                        </div>
                        <div class="col-lg-9">
                            <input type="file" class="form-control" name="file" value="{{ $reimbursement->file }}" />
                        </div>
                    </div>
                </div>
                <div class="col-12 d-flex flex-row justify-content-end mt-4">
                    <button type="reset" class="btn btn-danger mr-2">Batal</button>
                    <button type="submit" class="btn btn-primary mr-2 update_reimbursement">Simpan</button>
                </div>
            </div>
        @elseif (auth()->user()->role_id == 1)
            <div class="card card-custom pb-5 mt-10">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h2 class="card-label font-weight-bold">Data Reimbursement
                            <span class="d-block text-muted pt-2 font-size-sm">Beri persetujuan atau tolak pengajuan
                                reimbursement.</span>
                        </h2>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group row align-items-center mt-2">
                        <div class="col-lg-3">
                            <h6 class="font-weight-bold">Persetujuan</h6>
                            <small>Aktifkan switch button untuk menyetujui.</small>
                        </div>
                        <div class="col-lg-9">
                            <span class="switch switch-icon mr-3">
                                <label>
                                    <input type="checkbox" class="switch-toggle switch-toggle-round"
                                        name="is_approved_by_director" @error('is_approved_by_director') error @enderror"
                                        name="is_approved_by_director" id="is_approved_by_director"
                                        value="{{ config('__constant.STATUS.ACTIVE') }}"
                                        @if (old('is_approved_by_director') == config('__constant.STATUS.ACTIVE')) checked="checked" @endif />
                                    <span></span>
                                </label>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row align-items-center mt-2">
                        <div class="col-lg-3">
                            <h6 class="font-weight-bold">Catatan</h6>
                        </div>
                        <div class="col-lg-9">
                            <textarea class="form-control" name="note_director" rows="6" cols="30" style="text-align: justify"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-12 d-flex flex-row justify-content-end mt-4">
                    <button type="reset" class="btn btn-danger mr-2">Batal</button>
                    <button type="submit" class="btn btn-primary mr-2 update_reimbursement">Simpan</button>
                </div>
            </div>
        @else
            <div class="card card-custom pb-5 mt-10">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h2 class="card-label font-weight-bold">Data Reimbursement
                            <span class="d-block text-muted pt-2 font-size-sm">Beri persetujuan atau tolak pengajuan
                                reimbursement.</span>
                        </h2>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group row align-items-center mt-2">
                        <div class="col-lg-3">
                            <h6 class="font-weight-bold">Persetujuan</h6>
                            <small>Aktifkan switch button untuk menyetujui.</small>
                        </div>
                        <div class="col-lg-9">
                            <span class="switch switch-icon mr-3">
                                <label>
                                    <input type="checkbox" class="switch-toggle switch-toggle-round"
                                        name="is_approved_by_finance" @error('is_approved_by_finance') error @enderror"
                                        name="is_approved_by_finance" id="is_approved_by_finance"
                                        value="{{ config('__constant.STATUS.ACTIVE') }}"
                                        @if (old('is_approved_by_finance') == config('__constant.STATUS.ACTIVE')) checked="checked" @endif />
                                    <span></span>
                                </label>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row align-items-center mt-2">
                        <div class="col-lg-3">
                            <h6 class="font-weight-bold">Catatan</h6>
                        </div>
                        <div class="col-lg-9">
                            <textarea class="form-control" name="note_finance" rows="6" cols="30" style="text-align: justify"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-12 d-flex flex-row justify-content-end mt-4">
                    <button type="reset" class="btn btn-danger mr-2">Batal</button>
                    <button type="submit" class="btn btn-primary mr-2 update_reimbursement">Simpan</button>
                </div>
            </div>
        @endif

    </form>
@endsection
