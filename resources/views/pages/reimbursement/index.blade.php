@extends('layouts.master')
@section('style')
    <link href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('contents')
    <!--begin::Notice-->
    <div class="col-lg-12 mt-40">
        <div class="col-12">
            <div class="content-padding d-flex justify-content-start align-items-center mb-3">
                <div class="input-icon input-icon-right">
                    <input type="text" value="{{ request()->get('keyword') }}" class="form-control" name="search_field"
                        placeholder="Cari data reimbursement..." />
                    <span class="search_btn_container" id="search_btn"><i class="fas fa-search text-primary"></i></span>
                </div>
                @if (request()->get('keyword') != null)
                    <a href="/dashboard/reimbursement" class="ml-2"><span><i
                                class="fas fa-times text-danger"></i></span></a>
                @endif
            </div>
            @if (auth()->user()->role_id == 3)
                <a href="/dashboard/reimbursement/create" class="btn btn-light-primary font-weight-bolder">
                    <i class="fas fa-plus mr-2"></i>
                    Tambah Data Reimbursement</a>
            @endif
        </div>

        <div class="my-5">
            @include('layouts.alert')
        </div>

        @if (auth()->user()->role_id == 1 || auth()->user()->role_id == 2)
            <div class="col-12">
                <b>Note: Tekan Edit Button pada kolom Tindakan untuk mengatur persetujuan reimbursement.</b>
            </div>
        @endif
        <div class="card card-custom mt-5">
            <div class="card-body">
                <!--begin: Datatable-->
                <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Nama</th>
                            <th>Deskripsi</th>
                            <th>File Pendukung</th>
                            <th>Diajukan oleh</th>
                            <th>Disetujui/ditolak direktur</th>
                            <th>Catatan direktur direktur</th>
                            <th>Disetujui/ditolak finance</th>
                            <th>Catatan finance</th>
                            <th>Tindakan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($reimbursements as $reimbursement)
                            <tr>
                                <td>{{ \Carbon\Carbon::parse($reimbursement->date)->format('d/m/y H:i:s') }}</td>
                                <td>{{ $reimbursement->name }}</td>
                                <td>{{ $reimbursement->desc == null ? '-' : $reimbursement->desc }}</td>
                                <td class="text-center">
                                    @if ($reimbursement->file != null)
                                        <a href="/dashboard/reimbursement/downloadFile/{{ $reimbursement->id }}"
                                            class="mr-6" data-toggle="tooltip" title="Unduh File">
                                            <i class="fas fa-download text-primary"></i>
                                        </a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>{{ $reimbursement->staff->name }}</td>
                                <td>
                                    @if ($reimbursement->is_approved_by_director == 1 && $reimbursement->approved_by_director != null)
                                        <p><b class="text-success">Disetujui oleh {{ $reimbursement->director->name }}</b>
                                        </p>
                                    @elseif($reimbursement->is_approved_by_director == 0 && $reimbursement->approved_by_director != null)
                                        <p><b class="text-danger">Ditolak oleh {{ $reimbursement->director->name }}</b>
                                        </p>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>{{ $reimbursement->note_director == null ? '-' : $reimbursement->note_director }}</td>
                                <td>
                                    @if ($reimbursement->is_approved_by_finance == 1 && $reimbursement->approved_by_finance != null)
                                        <p><b class="text-success">Disetujui oleh {{ $reimbursement->finance->name }}</b>
                                        </p>
                                    @elseif($reimbursement->is_approved_by_finance == 0 && $reimbursement->approved_by_finance != null)
                                        <p><b class="text-danger">Ditolak oleh {{ $reimbursement->finance->name }}</b></p>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>{{ $reimbursement->note_finance == null ? '-' : $reimbursement->note_finance }}</td>
                                <td>
                                    <div class="d-flex flex-row">
                                        <a href="/dashboard/reimbursement/edit/{{ $reimbursement->id }}" class="mr-6"
                                            data-toggle="tooltip" title="Edit">
                                            <i class="fas fa-edit text-primary"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
        </div>
    </div>
@endsection
@section('additional_scripts')
    <script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
    <script>
        let _token = $('meta[name="csrf-token"]').attr('content');
        $("input[name='search_field']").keyup(function(e) {
            if (e.keyCode == 13) {
                var search_val = $("input[name='search_field']").val();
                window.location.href =
                    `/dashboard/reimbursement?keyword=${search_val}`;
            }
        });
        $('#search_btn').click(function(e) {
            var search_val = $("input[name='search_field']").val();
            window.location.href =
                `/dashboard/reimbursement?keyword=${search_val}`;
        })
    </script>
@endsection
