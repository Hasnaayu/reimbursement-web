@extends('layouts.master')
@section('contents')
    <form class="form content-padding" method="POST" id="add_reimbursement"
        action="{{ url('/dashboard/reimbursement/store') }}" enctype="multipart/form-data">
        @csrf

        @include('layouts.alert')

        <div class="card card-custom pb-5 mt-10">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h2 class="card-label font-weight-bold">Data Reimbursement
                        <span class="d-block text-muted pt-2 font-size-sm">Masukkan data reimbursement baru</span>
                    </h2>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group row align-items-center mt-2">
                    <div class="col-lg-3">
                        <h6 class="font-weight-bold">Tanggal</h6>
                    </div>
                    <div class="col-lg-9">
                        <div class="input-group date" id="kt_datetimepicker_2" data-target-input="nearest">
                            <div class="input-group-prepend" data-target="#kt_datetimepicker_2"
                                data-toggle="datetimepicker">
                                <span class="input-group-text" style="background-color: transparent">
                                    <i class="ki ki-calendar base_color"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control-datetime datetimepicker-input"
                                placeholder="Select date & time" data-target="#kt_datetimepicker_2" name="date"
                                required />
                        </div>
                    </div>
                </div>
                <div class="form-group row align-items-center mt-2">
                    <div class="col-lg-3">
                        <h6 class="font-weight-bold">Nama</h6>
                    </div>
                    <div class="col-lg-9">
                        <input type="text" name="name" class="form-control" placeholder="Masukkan nama dengan benar"
                            required />
                    </div>
                </div>
                <div class="form-group row align-items-center mt-2">
                    <div class="col-lg-3">
                        <h6 class="font-weight-bold">Deskripsi</h6>
                    </div>
                    <div class="col-lg-9">
                        <textarea class="form-control" name="desc" rows="6" cols="30" style="text-align: justify"></textarea>
                    </div>
                </div>
                <div class="form-group row align-items-center mt-2">
                    <div class="col-lg-3">
                        <h6 class="font-weight-bold">File Pendukung</h6>
                    </div>
                    <div class="col-lg-9">
                        <input type="file" class="form-control" name="file" />
                    </div>
                </div>
            </div>
            <div class="col-12 d-flex flex-row justify-content-end mt-4">
                <button type="reset" class="btn btn-danger mr-2">Batal</button>
                <button type="submit" class="btn btn-primary mr-2 add_reimbursement">Simpan</button>
            </div>
        </div>

    </form>
@endsection
@section('additional_scripts')
    <script type="text/javascript">
        let _token = $('meta[name="csrf-token"]').attr('content');
        validation = FormValidation.formValidation(
            KTUtil.getById('add_reimbursement'), {
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Nama reimbursement harus diisi'
                            },
                        }
                    },
                    date: {
                        validators: {
                            notEmpty: {
                                message: 'Tanggal harus diisi'
                            },

                        }
                    },
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    //defaultSubmit: new FormValidation.plugins.DefaultSubmit(), // Uncomment this line to enable normal button submit after form validation
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        );

        $('.add_reimbursement').on('click', function(e) {
            e.preventDefault();
            validation.validate().then(function(status) {
                if (status == 'Valid') {
                    $('#add_reimbursement').submit();
                } else {
                    swal.fire({
                        text: "Mohon isi data dengan benar.",
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Ok",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light-primary"
                        }
                    }).then(function() {
                        KTUtil.scrollTop();
                    });
                }
            });
        });
    </script>
@endsection
