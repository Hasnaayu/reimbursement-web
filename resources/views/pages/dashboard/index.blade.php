@extends('layouts.master')
@section('contents')
    <div class="col-lg-12 mt-40">
        <div class="row mb-4">
            <div class="col-lg-4 col-6">
                <a href="/dashboard/staff" style="text-decoration: none">
                    <div class="card card-custom gutter-b base-border bg-primary-o-15">
                        <div class="card-body base-text font-weight-boldest">
                            <div class="display-3 mb-3">
                                {{ $staff }}</div>
                            <h5>
                                Karyawan
                            </h5>
                        </div>
                    </div>
                </a>
            </div>
            @if (auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                <div class="col-lg-4 col-6">
                    <a href="/dashboard/reimbursement" style="text-decoration: none">
                        <div class="card card-custom gutter-b base-border bg-primary-o-15">
                            <div class="card-body base-text font-weight-boldest">
                                <div class="display-3 mb-3">
                                    {{ $unapproved_director }}</div>
                                <h5>
                                    Menunggu Persetujuan Direktur
                                </h5>
                            </div>
                        </div>
                    </a>
                </div>
            @endif
            <div class="col-lg-4 col-6">
                <a href="/dashboard/reimbursement" style="text-decoration: none">
                    <div class="card card-custom gutter-b base-border bg-primary-o-15">
                        <div class="card-body base-text font-weight-boldest">
                            <div class="display-3 mb-3">
                                {{ $unapproved_finance }}</div>
                            <h5>
                                Menunggu Persetujuan Finance
                            </h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-6">
                <a href="/dashboard/reimbursement" style="text-decoration: none">
                    <div class="card card-custom gutter-b base-border bg-primary-o-15">
                        <div class="card-body base-text font-weight-boldest">
                            <div class="display-3 mb-3">
                                {{ $approved_director }}</div>
                            <h5>
                                Disetujui Direktur
                            </h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-6">
                <a href="/dashboard/reimbursement" style="text-decoration: none">
                    <div class="card card-custom gutter-b base-border bg-primary-o-15">
                        <div class="card-body base-text font-weight-boldest">
                            <div class="display-3 mb-3">
                                {{ $approved_finance }}</div>
                            <h5>
                                Disetujui Finance
                            </h5>
                        </div>
                    </div>
                </a>
            </div>
            @if (auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                <div class="col-lg-4 col-6">
                    <a href="/dashboard/reimbursement" style="text-decoration: none">
                        <div class="card card-custom gutter-b base-border bg-primary-o-15">
                            <div class="card-body base-text font-weight-boldest">
                                <div class="display-3 mb-3">
                                    {{ $rejected_director }}</div>
                                <h5>
                                    Ditolak Direktur
                                </h5>
                            </div>
                        </div>
                    </a>
                </div>
            @endif
            <div class="col-lg-4 col-6">
                <a href="/dashboard/reimbursement" style="text-decoration: none">
                    <div class="card card-custom gutter-b base-border bg-primary-o-15">
                        <div class="card-body base-text font-weight-boldest">
                            <div class="display-3 mb-3">
                                {{ $rejected_finance }}</div>
                            <h5>
                                Ditolak Finance
                            </h5>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection
