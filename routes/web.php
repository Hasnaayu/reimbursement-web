<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', 'App\Http\Controllers\AuthController@login_view')->middleware('guest');

Route::prefix('dashboard')->namespace('App\Http\Controllers')->name('dashboard.')->group(
    static function () {
        Route::get('/login', 'AuthController@login_view')->name('login')->middleware('guest');
        Route::post('/login', 'AuthController@login')->name('login.post')->middleware('guest');
        Route::group(
            ['middleware' => ['auth']],
            function () {
                Route::get('/', 'DashboardController@index');
                Route::get('/logout', 'AuthController@logout')->name('logout');

                Route::prefix('staff')->name('staff.')->group(static function () {
                    Route::get('/', 'StaffController@index');
                    Route::get('/create', 'StaffController@create');
                    Route::post('/store', 'StaffController@store');
                    Route::get('/edit/{id}', 'StaffController@edit');
                    Route::put('/update/{id}', 'StaffController@update');
                    Route::put('/change-status/{id}', 'StaffController@change_status');
                    Route::delete('/delete/{id}', 'StaffController@delete');
                });

                Route::prefix('reimbursement')->name('reimbursement.')->group(static function () {
                    Route::get('/', 'ReimbursementController@index');
                    Route::get('/create', 'ReimbursementController@create');
                    Route::post('/store', 'ReimbursementController@store');
                    Route::get('/edit/{id}', 'ReimbursementController@edit');
                    Route::put('/update/{id}', 'ReimbursementController@update');
                    Route::get('/downloadFile/{id}', 'ReimbursementController@downloadFile');
                });
            }
        );
    }
);
