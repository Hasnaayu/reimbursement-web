<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Reimbursement;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $staff = User::whereIn('role_id', [2, 3])->count();
        $unapproved_director = Reimbursement::where('is_approved_by_director', null)
            ->count();
        $unapproved_finance = Reimbursement::where('is_approved_by_director', 1)
            ->where('is_approved_by_finance', null)
            ->count();
        $approved_director = Reimbursement::where('is_approved_by_director', 1)
            ->count();
        $approved_finance = Reimbursement::where('is_approved_by_finance', 1)
            ->count();
        $rejected_director = Reimbursement::where('is_approved_by_director', 0)
            ->count();
        $rejected_finance = Reimbursement::where('is_approved_by_finance', 0)
            ->count();
        return view('pages.dashboard.index', [
            'staff' => $staff,
            'unapproved_director' => $unapproved_director,
            'unapproved_finance' => $unapproved_finance,
            'approved_director' => $approved_director,
            'approved_finance' => $approved_finance,
            'rejected_director' => $rejected_director,
            'rejected_finance' => $rejected_finance,
            'title' => 'Dashboard'
        ]);
    }
}
