<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Reimbursement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ReimbursementController extends Controller
{
    public function index(Request $request)
    {
        if (auth()->user()->role_id == 3 || auth()->user()->role_id == 1) {
            if (isset($request->keyword)) {
                $reimbursements = Reimbursement::with(['staff', 'director', 'finance'])
                    ->where('name', 'ilike', '%' . $request->keyword . '%')
                    ->get();
            } else {
                $reimbursements = Reimbursement::with(['staff', 'director', 'finance'])
                    ->get();
            }
        } else {
            if (isset($request->keyword)) {
                $reimbursements = Reimbursement::with(['staff', 'director', 'finance'])
                    ->where('name', 'ilike', '%' . $request->keyword . '%')
                    ->where('is_approved_by_director', 1)
                    ->get();
            } else {
                $reimbursements = Reimbursement::with(['staff', 'director', 'finance'])
                    ->where('is_approved_by_director', 1)
                    ->get();
            }
        }
        return view('pages.reimbursement.index', ['reimbursements' => $reimbursements, 'title' => 'Daftar Data Reimbursement', 'desc' => 'Daftar Data Reimbursement']);
    }

    public function create()
    {
        return view('pages.reimbursement.create', [
            'title' => 'Tambah Data Reimbursement',
            'desc' => 'Tambah Data Reimbursement'
        ]);
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'date' => 'required',
                'name' => 'required'
            ]);

            if ($validator->fails()) {
                return redirect('/dashboard/reimbursement')->with('error', 'Data reimbursement gagal disimpan! Pastikan data yang dimasukkan benar.');
            } else {
                if ($request->file) {
                    $files = 'reimbursement-' . time() . '.' . $request->file->getClientOriginalExtension();
                    $request->file->move(public_path('/files'), $files);
                }
                $reimbursement = Reimbursement::create([
                    'date' => Carbon::parse($request->date),
                    'name' => $request->name,
                    'desc' => $request->desc,
                    'file' => $files,
                    'created_by_staff' => auth()->user()->id
                ]);
                return redirect('/dashboard/reimbursement')->with('success', 'Data reimbursement berhasil disimpan!');
            }
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', 'Data reimbursement gagal disimpan!');
        }
    }

    public function edit($id)
    {
        $reimbursement = Reimbursement::where('id', $id)->first();
        return view('pages.reimbursement.edit', [
            'reimbursement' => $reimbursement,
            'title' => 'Tambah Data Reimbursement',
            'desc' => 'Tambah Data Reimbursement'
        ]);
    }

    public function update(Request $request, $id)
    {
        try {
            if (auth()->user()->role_id == 3) {
                $validator = Validator::make($request->all(), [
                    'date' => 'required',
                    'name' => 'required'
                ]);

                if ($validator->fails()) {
                    return redirect('/dashboard/reimbursement')->with('error', 'Data reimbursement gagal diperbarui! Pastikan data yang dimasukkan benar.');
                } else {
                    if ($request->file) {
                        $files = 'reimbursement-' . time() . '.' . $request->file->getClientOriginalExtension();
                        $request->file->move(public_path('/files'), $files);
                    }
                    $reimbursement = DB::table('reimbursements')->where('id', $id)->update([
                        'date' => Carbon::parse($request->date),
                        'name' => $request->name,
                        'desc' => $request->desc,
                        'file' => $files,
                        'created_by_staff' => auth()->user()->id,
                        'is_approved_by_director' => null,
                        'approved_by_director' => null,
                        'note_director' => null,
                        'is_approved_by_finance' => null,
                        'approved_by_finance' => null,
                        'note_finance' => null,
                    ]);
                    return redirect('/dashboard/reimbursement')->with('success', 'Data reimbursement berhasil diperbarui!');
                }
            } elseif (auth()->user()->role_id == 1) {
                $reimbursement = DB::table('reimbursements')->where('id', $id)->update([
                    'is_approved_by_director' =>
                    $request->input('is_approved_by_director') == config('__constant.STATUS.ACTIVE') ?
                        config('__constant.STATUS.ACTIVE') :
                        config('__constant.STATUS.INACTIVE'),
                    'note_director' => $request->note_director,
                    'approved_by_director' => auth()->user()->id
                ]);
                return redirect('/dashboard/reimbursement')->with('success', 'Data reimbursement berhasil diperbarui!');
            } else {
                $reimbursement = DB::table('reimbursements')->where('id', $id)->update([
                    'is_approved_by_finance' =>
                    $request->input('is_approved_by_finance') == config('__constant.STATUS.ACTIVE') ?
                        config('__constant.STATUS.ACTIVE') :
                        config('__constant.STATUS.INACTIVE'),
                    'note_finance' => $request->note_finance,
                    'approved_by_finance' => auth()->user()->id
                ]);
                return redirect('/dashboard/reimbursement')->with('success', 'Data reimbursement berhasil diperbarui!');
            }
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', 'Data reimbursement gagal diperbarui!');
        }
    }

    public function downloadFile($id)
    {
        $reimbursement = Reimbursement::where('id', $id)->first();
        $filepath = public_path('files/' . $reimbursement->file);

        return Response::download($filepath);
    }
}
