<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class StaffController extends Controller
{
    public function index(Request $request)
    {
        if (isset($request->keyword)) {
            $users = User::whereHas('role', function ($query) {
                $query->whereIn('name', ['Staff', 'Finance']);
            })
                ->where('name', 'ilike', '%' . $request->keyword . '%')
                ->get();
        } else {
            $users = User::whereHas('role', function ($query) {
                $query->whereIn('name', ['Staff', 'Finance']);
            })->get();
        }

        return view('pages.staff.index', ['users' => $users, 'title' => 'Daftar Karyawan', 'desc' => 'Daftar Karyawan']);
    }

    public function create()
    {
        if (Gate::allows('manage-employees')) {
            $roles = Role::whereIn('name', ['Staff', 'Finance'])->get();
            return view('pages.staff.create', [
                'roles' => $roles,
                'title' => 'Tambah Data Karyawan',
                'desc' => 'Tambah Data Karyawan'
            ]);
        } else {
            return redirect()->back()->with('error', 'Anda tidak memiliki hak akses pada fitur ini.');
        }
    }

    public function store(Request $request)
    {
        if (Gate::allows('manage-employees')) {
            try {
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'role_id' => 'required',
                    'nip' => 'required'
                ]);

                if ($validator->fails()) {
                    return redirect('/dashboard/staff')->with('error', 'Data karyawan gagal disimpan! Pastikan data yang dimasukkan benar.');
                } else {
                    $existing_user = User::where('nip', $request->nip)
                        ->select('id')->first();
                    if (!$existing_user) {
                        $user = User::create([
                            'name' => $request->name,
                            'email' => $request->email,
                            'nip' => $request->nip,
                            'role_id' => $request->role_id,
                            'is_active' => 1,
                            'password' => Hash::make($request->password)
                        ]);
                        return redirect('/dashboard/staff')->with('success', 'Data Karyawan berhasil disimpan!');
                    } else {
                        return redirect()->back()->with('error', 'NIP sudah terdaftar! Masukkan NIP lain yang belum terdaftar!');
                    }
                }
            } catch (\Exception $exception) {
                return redirect()->back()->with('error', 'Data Karyawan gagal disimpan!');
            }
        } else {
            return redirect()->back()->with('error', 'Anda tidak memiliki hak akses pada fitur ini.');
        }
    }

    public function edit($id)
    {
        if (Gate::allows('manage-employees')) {
            $user = User::where('id', $id)->first();
            $roles = Role::whereIn('name', ['Staff', 'Finance'])->get();
            return view('pages.staff.edit', [
                'user' => $user,
                'roles' => $roles,
                'title' => 'Edit Data Karyawan',
                'desc' => 'Edit Data Karyawan'
            ]);
        } else {
            return redirect()->back()->with('error', 'Anda tidak memiliki hak akses pada fitur ini.');
        }
    }

    public function update(Request $request, $id)
    {
        if (Gate::allows('manage-employees')) {
            try {
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'role_id' => 'required',
                    'nip' => 'required',
                ]);
                if ($validator->fails()) {
                    return redirect('/dashboard/staff')->with('error', 'Data karyawan gagal disimpan! Pastikan data yang dimasukkan benar dan lengkap atau NIP belum terdaftar sebelumnya.');
                } else {
                    $existing_user = User::where('nip', $request->nip)
                        ->where('id', '!=', $id)
                        ->select('id')
                        ->first();
                    if (!$existing_user) {
                        $user = DB::table('users')->where('id', $id)->update([
                            'name' => $request->name,
                            'email' => $request->email,
                            'nip' => $request->nip,
                            'role_id' => $request->role_id
                        ]);
                        return redirect('/dashboard/staff')->with('success', 'Data Karyawan berhasil diperbarui!');
                    } else {
                        return redirect()->back()->with('error', 'NIP sudah terdaftar! Masukkan NIP lain yang belum terdaftar!');
                    }
                }
            } catch (\Exception $exception) {
                return redirect()->back()->with('error', 'Data Karyawan gagal diperbarui!');
            }
        } else {
            return redirect()->back()->with('error', 'Anda tidak memiliki hak akses pada fitur ini.');
        }
    }

    public function change_status($id)
    {
        if (Gate::allows('manage-employees')) {
            try {
                $user = User::where('id', $id)
                    ->select('is_active')
                    ->first();
                if ($user->is_active == true) {
                    $is_active = false;
                } else {
                    $is_active = true;
                }
                $values = array('is_active' => $is_active);
                User::where('id', $id)->update($values);
                return response()->json([
                    'success' => true,
                    'message' => 'Status berhasil diubah!',
                    'user' => $user
                ]);
            } catch (\Exception $exception) {
                return response()->json([
                    'success' => true,
                    'message' => $exception->getMessage()
                ]);
            }
        } else {
            return redirect()->back()->with('error', 'Anda tidak memiliki hak akses pada fitur ini.');
        }
    }

    public function delete($id)
    {
        if (Gate::allows('manage-employees')) {
            $user = User::where('id', $id)->first();
            $user->delete();
            return response()->json([
                'success' => true,
                'message' => 'Berhasil menghapus data karyawan!'
            ]);
        } else {
            return redirect()->back()->with('error', 'Anda tidak memiliki hak akses pada fitur ini.');
        }
    }
}
