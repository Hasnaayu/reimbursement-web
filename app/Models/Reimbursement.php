<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reimbursement extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function staff()
    {
        return $this->belongsTo(User::class, 'created_by_staff', 'id');
    }

    public function director()
    {
        return $this->belongsTo(User::class, 'approved_by_director', 'id');
    }

    public function finance()
    {
        return $this->belongsTo(User::class, 'approved_by_finance', 'id');
    }
}